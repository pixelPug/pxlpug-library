using System;

namespace PxlPug.Editor
{
  public interface ISyncLink
  {
    SyncFile[] GetFiles();
  }
  
  public struct SyncFile
  {
    public Type jsonType;
    public Type scriptableType;

    public SyncFile(Type jType, Type sType)
    {
      jsonType = jType;
      scriptableType = sType;
    }
  }
}