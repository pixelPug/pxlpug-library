using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

using Object = UnityEngine.Object;

#if UNITY_EDITOR

namespace PxlPug.Editor
{
  public class AssetDatabaseHelper
  {
    public static string FindAssetPath(string name)
    {
      var guids = AssetDatabase.FindAssets(name);
      return AssetDatabase.GUIDToAssetPath(guids[0]);
    }
    
    public static List<T> FindAssetsByType<T>() where T : Object
    {
      List<T> assets = new List<T>();
      string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
      for(int i = 0; i < guids.Length; i++)
      {
        var asset = AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guids[i]));
        if(asset != null)
          assets.Add(asset);
      }
      return assets;
    }
    
    public static T FindAsset<T>(string name) where T : Object
    {
      var guids = AssetDatabase.FindAssets(name + " t:" + typeof(T));
      return AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guids[0]));
    }

    public static Object FindAsset(string name, Type type)
    {
      var guids = AssetDatabase.FindAssets(name);
      return AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guids[0]), type);
    }

    public static T FindScriptableObject<T>(string name) where T : ScriptableObject
    {
      var guids = AssetDatabase.FindAssets(name + " t:ScriptableObject");
      return AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guids[0]));
    }

    public static void SaveAsset<T>(T ob) where T : Object
    {
      EditorUtility.SetDirty(ob);
      AssetDatabase.SaveAssets();
      AssetDatabase.Refresh();
    }
  }
}

#endif