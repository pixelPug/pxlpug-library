using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR

namespace PxlPug.Editor.ProjectWindow
{
  public class SceneExplorer
  {
    [Serializable]
    public class State
    {
      public EditorBuildSettingsScene[] scenes;
      public string[] sceneNames;
      public bool scenesFoldOutOpen = true;
    }

    protected EditorBuildSettingsScene[] _editorScenes;
    protected List<string> _lockedScenes;
    protected readonly State _state;
    
    
    public SceneExplorer(List<string> lockedScenes)
    {
      _lockedScenes = lockedScenes;
      _state = new State();
      PopulateScenes();
    }
    
    protected virtual void PopulateScenes()
    {
      _state.scenes = EditorBuildSettings.scenes;
      _state.sceneNames = new string[_state.scenes.Length];
      for(int i = 0; i < _state.scenes.Length; i++)
      {
        _state.sceneNames[i] = GetSceneName(_state.scenes[i].path);
      }
    }

    protected virtual string GetSceneName(string path)
    {
      var startIndex = Mathf.Max(path.LastIndexOf("/", StringComparison.Ordinal) + 1, 0);
      var endIndex = path.LastIndexOf(".", StringComparison.Ordinal);
      if(endIndex < 0) endIndex = path.Length;
      var sceneName = path.Substring(startIndex, endIndex - startIndex);
      return sceneName;
    }

    public virtual void GuiRender(ProjectWindowStyle style)
    {
      if(ShouldRepopulateScenes())
        PopulateScenes();
      
      _state.scenesFoldOutOpen = EditorGUILayout.Foldout(_state.scenesFoldOutOpen, "Scenes");
      if(!_state.scenesFoldOutOpen)
        return;

      DrawScenes(_state.scenes, _state.sceneNames, style);
      
      if(_state.scenes.Length ==  EditorBuildSettings.scenes.Length)
        EditorBuildSettings.scenes = _state.scenes;
    }
    
    protected virtual bool ShouldRepopulateScenes()
    {
      _editorScenes = EditorBuildSettings.scenes;
      if(_state.scenes.Length != _editorScenes.Length)
        return true;

      for(int i = 0; i < _editorScenes.Length; i++)
      {
        if(_editorScenes[i] != _state.scenes[i])
          return true;
      }
      return false;
    }

    protected virtual void DrawScenes(EditorBuildSettingsScene[] scenes, string[] sceneNames, ProjectWindowStyle style)
    {
      var activeScene = SceneManager.GetActiveScene();

      EditorGUILayout.BeginVertical(style.accordian);

      for(int i = 0; i < scenes.Length; i++)
      {
        EditorGUILayout.BeginHorizontal();

        EditorGUI.BeginDisabledGroup(_lockedScenes.Contains(sceneNames[i]));

        var isActiveScene = scenes[i].path == activeScene.path;
        if(isActiveScene)
          _state.scenes[i].enabled = EditorGUILayout.ToggleLeft(sceneNames[i], _state.scenes[i].enabled, style.toggle);
        else
          _state.scenes[i].enabled = EditorGUILayout.ToggleLeft(sceneNames[i], _state.scenes[i].enabled);

        EditorGUI.EndDisabledGroup();

        EditorGUI.BeginDisabledGroup(isActiveScene);
        if(GUILayout.Button("Open"))
          UnityEditor.SceneManagement.EditorSceneManager.OpenScene(_state.scenes[i].path);
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.EndHorizontal();
      }

      EditorGUILayout.EndVertical();      
    }

    public int GetSceneIndex(string sceneName)
    {
      for(int i = 0; i < _state.sceneNames.Length; i++)
      {
        if(_state.sceneNames[i].Equals(sceneName))
          return i;
      }
      return -1;
    }
    
    public string GetOpenSceneName()
    {
      return SceneManager.GetActiveScene().name;
    }
    
    public bool IsProjectScene(string sceneName)
    {
      return _state.sceneNames.Contains(sceneName);
    }

    public string GetScenePath(int index)
    {
      return _state.scenes[0].path;
    }
  }
}

#endif