using System;
using System.Collections;
using System.Text;
using PxlPug.Extensions;
using PxlPug.Math;
using PxlPug.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VContainer;
using VContainer.Unity;

namespace PxlPug.Debug
{
  public class TerminalView : IInitializable
  {
    [Serializable]
    public class Settings
    {
      public float height = 300f;
      public Color backgroundColor = new Color(0,0,0, 0.9f);
      public Color textColor = Color.white;
      public Color warningColor = new Color(0.992f, 0.447f, 0.000f);
      public Color errorColor = Color.red;
      public Color buttonColor = new Color(0.18f, 0.18f, 0.18f);
      public Color buttonTextColor = new Color(0.62f, 0.62f, 0.62f);
      public Color promptCharacterColor = new Color(0.4f, 0.4f, 0.4f);
      public Color caretColor = Color.cyan;
      public float textSize = 16f;
      public float buttonTextSize = 16f;
      public float scrollbarThickness = 15f;
      public string promptCharacter = ">";
      public bool showButton = true;
      public float topBorder = 14f;
      public float leftBorder = 14f;
      public float rightBorder = 14f;
      public float bottomBorder = 14f;
      public bool animate = true;
      public float animationTime = 0.4f;
    }
    
    public event Action<string> OnInputSubmit = delegate { };

    private Settings _settings;
    private GameObject _view;
    private RectTransform _rectTransform;
    private Button _enterButton;
    private TMP_InputField _inputField;
    private ScrollRect _scrollView;
    private Scrollbar _scrollbar;
    private TextMeshProUGUI _historyLog;
    private StringBuilder _stringBuilder;
    private Coroutine _tweenRoutine;
    private Image _bg;
    private GameObject _currentFocus;
    private GameObject _sceneFocus;
    private bool _hasFocus;
    private bool _setupComplete;

    private string _caretHexColor;
    private string _warningHexColor;
    private string _errorHexColor;

    [Inject]
    public TerminalView(Settings settings)
    {
      _settings = settings;
    }
    
    public void Initialize()
    {
      _stringBuilder = new StringBuilder();
      BuildView();
      AddListeners();
      _view.SetActive(false);
      _setupComplete = true;
    }

    private void BuildView()
    {
      _view = new GameObject("Terminal");
      _view.AddComponent<DontDestroy>();
      _view.layer = LayerMask.NameToLayer("UI");
      DefineColors();
      CreateCanvas();
      CreatePanel();
      CreateEnterButton();
      CreateScrollView();
      CreatePrompt();
      CreateInputField();
    }

    private void DefineColors()
    {
      _caretHexColor = ColorUtility.ToHtmlStringRGB(_settings.caretColor);
      _warningHexColor = ColorUtility.ToHtmlStringRGB(_settings.warningColor);
      _errorHexColor = ColorUtility.ToHtmlStringRGB(_settings.errorColor);
    }

    private void CreateCanvas()
    {
      var canvas = _view.AddComponent<Canvas>();
      canvas.renderMode = RenderMode.ScreenSpaceOverlay;
      canvas.sortingOrder = 100;

      var canvasScaler = _view.AddComponent<CanvasScaler>();
      canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
      canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
      canvasScaler.referenceResolution = new Vector2(800, 600);

      var raycaster = _view.AddComponent<GraphicRaycaster>();
      raycaster.blockingObjects = GraphicRaycaster.BlockingObjects.None;
    }

    private void CreatePanel()
    {
      _bg = CreateChildComponent<Image>("Panel", _view.transform);
      _bg.color = _settings.backgroundColor;
      _rectTransform = _bg.GetComponent<RectTransform>();
      
      // Anchored Top Middle & Stretching Horizontally
      _rectTransform.SetAnchorsAndOffsets(new Vector2(0,1f), Vector2.one, Vector2.zero, Vector2.zero);
      _rectTransform.pivot = new Vector2(0.5f, 1f);
      _rectTransform.anchoredPosition = Vector2.zero;
      _rectTransform.SetHeight(_settings.height);
    }
    
    private T CreateChildComponent<T>(string name, Transform parent) where T: Component
    {
      var ob = new GameObject(name);
      ob.transform.SetParent(parent);
      return ob.AddComponent<T>();
    }

    private void CreateScrollView()
    {
      _scrollView = CreateChildComponent<ScrollRect>("ScrollView", _bg.rectTransform);
      var scrollRect = _scrollView.GetComponent<RectTransform>();
      scrollRect.pivot = new Vector2(0.5f, 0);
      scrollRect.SetAnchorsAndOffsets(Vector2.zero, Vector2.one, new Vector2(_settings.leftBorder, 70f), new Vector2(-_settings.rightBorder, -_settings.topBorder));

      var viewport = CreateChildComponent<Image>("Viewport", scrollRect);
      viewport.color = new Color(0, 0, 0, 0.004f);
      viewport.rectTransform.pivot = new Vector2(0, 1f);
      viewport.rectTransform.anchorMin = Vector2.zero;
      viewport.rectTransform.anchorMax = Vector2.one;
      viewport.rectTransform.offsetMin = Vector2.zero;
      viewport.rectTransform.offsetMax = new Vector2(-_settings.rightBorder, 0);
      viewport.gameObject.AddComponent<Mask>();
      _scrollView.viewport = viewport.rectTransform;

      _historyLog = CreateChildComponent<TextMeshProUGUI>("Content", viewport.rectTransform);
      _historyLog.rectTransform.pivot = new Vector2(0.5f, 1f);
      _historyLog.rectTransform.anchorMin = new Vector2(0, 1f);
      _historyLog.rectTransform.anchorMax = new Vector2(1f, 1f);
      _historyLog.rectTransform.offsetMin = Vector2.zero;
      _historyLog.rectTransform.offsetMax = Vector2.zero;
      _historyLog.rectTransform.SetHeight(1000f);
      _historyLog.fontSize = _settings.textSize;
      _historyLog.color = _settings.textColor;
      _scrollView.content = _historyLog.rectTransform;

      _historyLog.GetOrAddComponent<VerticalLayoutGroup>();
      var fitter = _historyLog.GetOrAddComponent<ContentSizeFitter>();
      fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

      var horizontalScollbar = CreateScrollbar("HorizontalScrollbar", Scrollbar.Direction.LeftToRight, scrollRect);
      _scrollView.horizontalScrollbar = horizontalScollbar;
      _scrollView.horizontalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;

      var verticalScrollbar = CreateScrollbar("VerticalScrollbar", Scrollbar.Direction.BottomToTop, scrollRect);
      _scrollView.verticalScrollbar = verticalScrollbar;
      _scrollView.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;
    }
    
    private Scrollbar CreateScrollbar(string name, Scrollbar.Direction dir, Transform parent)
    {
      _scrollbar = CreateChildComponent<Scrollbar>(name, parent);
      var rect = _scrollbar.gameObject.GetComponent<RectTransform>();
      var scrollImage = _scrollbar.gameObject.AddComponent<Image>();
      scrollImage.color = ColorUtil.FromHex("272727");

      var slidingArea = CreateChildComponent<Image>("Sliding Area", rect);
      slidingArea.enabled = false;
      var slidingRect = slidingArea.GetComponent<RectTransform>();
      slidingRect.anchorMin = Vector2.zero;
      slidingRect.anchorMax = Vector2.one;
      slidingRect.offsetMin = new Vector2(10f, 10f);
      slidingRect.offsetMax = new Vector2(-10f, -10f);

      var handle = CreateChildComponent<Image>("Handle", slidingArea.transform);
      handle.rectTransform.offsetMin = new Vector2(-10f, -10f);
      handle.rectTransform.offsetMax = new Vector2(10f, 10f);
      _scrollbar.targetGraphic = handle;
      _scrollbar.handleRect = handle.rectTransform;
      _scrollbar.targetGraphic.color = ColorUtil.FromHex("4D4D4D");
      
      _scrollbar.direction = dir;
      if(dir == Scrollbar.Direction.LeftToRight)
      {
        rect.pivot = Vector2.zero;
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = new Vector2(1f, 0);
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = new Vector2(-_settings.rightBorder, 0);
        rect.SetHeight(_settings.scrollbarThickness);
      }
      else
      {
        rect.pivot =Vector2.one;
        rect.anchorMin = new Vector2(1f, 0);
        rect.anchorMax = Vector2.one;
        rect.offsetMin = new Vector2(0, _settings.topBorder);
        rect.offsetMax = Vector2.zero;
        rect.SetWidth(_settings.scrollbarThickness);
      }

      return _scrollbar;
    }
    
    private void CreatePrompt()
    {
      var prompt = CreateChildComponent<TextMeshProUGUI>("Prompt", _bg.rectTransform);
      prompt.SetText(_settings.promptCharacter);
      prompt.color = _settings.promptCharacterColor;
      prompt.fontSize = _settings.textSize;
      prompt.fontStyle = FontStyles.Bold;
      prompt.alignment = TextAlignmentOptions.Left;

      prompt.rectTransform.pivot = Vector2.zero;
      prompt.rectTransform.anchorMin = Vector2.zero;
      prompt.rectTransform.anchorMax = Vector2.zero;
      prompt.rectTransform.SetWidth(20f);
      prompt.rectTransform.SetHeight(40f);
      prompt.rectTransform.anchoredPosition = new Vector2(_settings.leftBorder, _settings.bottomBorder);
    }
    
    private void CreateEnterButton()
    {
      _enterButton = CreateChildComponent<Button>("EnterButton", _bg.rectTransform);
      var image = _enterButton.gameObject.AddComponent<Image>();
      image.color = _settings.buttonColor;
      _enterButton.image = image;
      if(!_settings.showButton)
        _enterButton.gameObject.SetActive(false);
      
      var buttonRect = _enterButton.GetComponent<RectTransform>();
      buttonRect.pivot = new Vector2(1f, 0);
      buttonRect.anchorMin = new Vector2(1f, 0);
      buttonRect.anchorMax = new Vector2(1f, 0);
      buttonRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 128f);
      buttonRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40f);
      buttonRect.anchoredPosition = new Vector2(-_settings.rightBorder, _settings.bottomBorder);

      var buttonText = CreateChildComponent<TextMeshProUGUI>("Text", _enterButton.transform);
      buttonText.rectTransform.SetAnchorsAndOffsets(Vector2.zero, Vector2.one, Vector2.zero, Vector2.zero);
      
      buttonText.SetText("ENTER");
      buttonText.fontSize = _settings.buttonTextSize;
      buttonText.fontStyle = FontStyles.Bold;
      buttonText.color = _settings.buttonTextColor;
      buttonText.alignment = TextAlignmentOptions.Center;
      buttonText.SetAllDirty();
    }
    
    private void CreateInputField()
    {
      var fieldImage = CreateChildComponent<Image>("InputField", _bg.rectTransform);
      fieldImage.enabled = false;
      var inputRect = fieldImage.rectTransform;
      inputRect.pivot = new Vector2(0.5f, 0);
      inputRect.anchorMin = Vector2.zero;
      inputRect.anchorMax = new Vector2(1f, 0);
      inputRect.offsetMin = new Vector2(30f, 14f);
      inputRect.offsetMax = new Vector2(-180f, 0);
      inputRect.SetHeight(40f);
      
      _inputField = fieldImage.gameObject.AddComponent<TMP_InputField>();
      _inputField.customCaretColor = true;
      _inputField.caretColor = _settings.caretColor;
      _inputField.caretWidth = 5;
      _inputField.isRichTextEditingAllowed = true;
      _inputField.transition = Selectable.Transition.None;

      var textArea = CreateChildComponent<RectMask2D>("Text Area", inputRect);
      StretchFull(textArea.rectTransform);
      
      var text = CreateChildComponent<TextMeshProUGUI>("Text", textArea.rectTransform);
      StretchFull(text.rectTransform);
      text.alignment = TextAlignmentOptions.Left;
      text.fontSize = _settings.textSize;
      
      _inputField.textViewport = textArea.rectTransform;
      _inputField.textComponent = text;
      _inputField.pointSize = _settings.textSize;
    }
    
    private void StretchFull(RectTransform rectTransform)
    {
      rectTransform.anchorMin = Vector2.zero;
      rectTransform.anchorMax = Vector2.one;
      rectTransform.offsetMin = Vector2.zero;
      rectTransform.offsetMax = Vector2.zero;
    }
    
    private void AddListeners()
    {
      _enterButton.onClick.AddListener(OnEnterButton);
      _inputField.onSubmit.AddListener(SubmitInput);
    }
    
    private void OnEnterButton()
    {
      SubmitInput(_inputField.text);
    }

    private void SubmitInput(string txt)
    {
      OnInputSubmit(txt);
    }

    public void SetCommand(string command)
    {
      _inputField.text = command;
      Scheduler.DelayInvoke(() => { _inputField.caretPosition = _inputField.text.Length; }, 1);
    }

    public void AddWarning(string txt)
    {
      AddOutput($"<color=#{_warningHexColor}>{txt}</color>");
    }

    public void AddError(string txt)
    {
      AddOutput($"<color=#{_errorHexColor}>{txt}</color>");
    }
    
    public void AddOutput(string txt)
    {
      _stringBuilder.AppendLine(txt);
      _historyLog.SetText(_stringBuilder.ToString());
      Scheduler.DelayInvoke(ScrollToBottom, 1);
    }
    
    private void ScrollToBottom()
    {
      _scrollView.verticalNormalizedPosition = 0f;
    }
    
    public void AddOutputCommand(string txt)
    {
      var prompt = $"<color=#{_caretHexColor}>{_settings.promptCharacter}</color>";
      _stringBuilder.AppendLine(prompt + " " + txt);
      _stringBuilder.AppendLine();
      _historyLog.SetText(_stringBuilder.ToString());
    }
    
    public void Clear()
    {
      _stringBuilder.Clear();
      _historyLog.SetText(_stringBuilder.ToString());
    }

    public void ClearInput()
    {
      _inputField.text = "";
      _inputField.caretPosition = 0;
      SetFocus();
    }
    
    public void LateTick()
    {
      if(!_hasFocus)
        return;

      _currentFocus = EventSystem.current.currentSelectedGameObject;
      if(ShouldSetSceneFocus())
        _sceneFocus = _currentFocus;
      
      if(_currentFocus != _inputField.gameObject)
       SetFocus();
    }
    
    private bool ShouldSetSceneFocus()
    {
      return _currentFocus != null && _currentFocus != _inputField.gameObject && _currentFocus != _scrollbar.gameObject 
        && _currentFocus != _enterButton.gameObject;
    }
    
    public void Show()
    {
      _sceneFocus = EventSystem.current.currentSelectedGameObject;
      if(_settings.animate)
      {
        CancelTween();
        _tweenRoutine = Scheduler.Instance.StartCoroutine(AnimateIn());
      }
      else
      {
        EnableFocus();
      }
      _view.SetActive(true);
    }
    
    private IEnumerator AnimateIn()
    {
      var yPos = _settings.height;
      _rectTransform.anchoredPosition = new Vector2(0, yPos);
      var elapsedTime = 0f;
      var progress = 0f;
      while(progress < 1f)
      {
        elapsedTime += GameTime.deltaTime;
        progress = elapsedTime / _settings.animationTime;
        yPos = Ease.InOutCubic(_settings.height, 0, progress);
        _rectTransform.anchoredPosition = new Vector2(0, yPos);
        yield return null;
      }
      _rectTransform.anchoredPosition = Vector2.zero;
      EnableFocus();
    }
    
    private void EnableFocus()
    {
      _enterButton.enabled = true;
      _inputField.enabled = true;
      Scheduler.DelayInvoke(SetFocus, 1);
    }
    
    public void SetFocus()
    {
      _inputField.OnPointerClick(new PointerEventData(EventSystem.current));
      _hasFocus = true;
    }
    
    public void Hide()
    {
      if(!_setupComplete)
      {
        Scheduler.StartRoutine(CallWhenSetupIsComplete(Hide));
        return;
      }
    
      _inputField.enabled = false;
      _enterButton.enabled = false;
      if(_settings.animate)
      {
        CancelTween();
        _tweenRoutine = Scheduler.StartRoutine(AnimateOut());
      }
      else
      {
        _view.SetActive(false);
      }

      _hasFocus = false;
      EventSystem.current?.SetSelectedGameObject(_sceneFocus);
    }
    
    private IEnumerator CallWhenSetupIsComplete(Action action)
    {
      while(!_setupComplete)
        yield return null;
      action.Invoke();
    }

    private void CancelTween()
    {
      if(_tweenRoutine != null)
        Scheduler.StopRoutine(_tweenRoutine);
    }
    
    private IEnumerator AnimateOut()
    {
      var yPos = 0f;
      _rectTransform.anchoredPosition = new Vector2(0, yPos);
      var elapsedTime = 0f;
      var progress = 0f;
      while(progress < 1f)
      {
        elapsedTime += GameTime.deltaTime;
        progress = elapsedTime / (_settings.animationTime * 0.5f);
        yPos = Ease.OutCubic(0f, _settings.height, progress);
        _rectTransform.anchoredPosition = new Vector2(0, yPos);
        yield return null;
      }
      _rectTransform.anchoredPosition = new Vector2(0, _settings.height);
      _view.SetActive(false);
    }
    
    public void Destroy()
    {
      RemoveListeners();
      _stringBuilder = null;
    }
    
    private void RemoveListeners()
    {
      _inputField.onSubmit.RemoveAllListeners();
      _enterButton.onClick.RemoveAllListeners();
    }
  }
}