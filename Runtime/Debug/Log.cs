using System.Collections.Generic;
using PxlPug.Extensions;

namespace PxlPug.Debug
{
  public enum LogType
  {
      Info,
      Warning,
      Error
  }
  
  public struct LogEntry
  {
    public string message;
    public LogType type;
    
    public LogEntry(string m, LogType t)
    {
      message = m;
      type = t;
    }
  }

  public class Log
  {
    public static List<LogEntry> history = new List<LogEntry>();
    public static bool logInfo = true;
    public static bool logWarnings = true;
    public static bool logErrors = true;

    
    public static void Info(object ob)
    {
      if(!logInfo)
        return;
      UnityEngine.Debug.Log(ob);
      var message = "".Fmt(ob);
      history.Add(new LogEntry(message, LogType.Info));
    }

    public static void Info(string message, params object[] args)
    {
      if(!logInfo)
        return;
      message = message.Fmt(args);
      UnityEngine.Debug.Log(message);
      history.Add(new LogEntry(message, LogType.Info));
    }

    public static void Warning(string message, params object[] args)
    {
      if(!logWarnings)
        return;
      message = message.Fmt(args);
      UnityEngine.Debug.LogWarning(message);
      history.Add(new LogEntry(message, LogType.Warning));
    }

    public static void Warning(object ob)
    {
      if(!logWarnings)
        return;
      UnityEngine.Debug.LogWarning(ob);
      var message = "".Fmt(ob);
      history.Add(new LogEntry(message, LogType.Warning));
    }
    
    public static void Error(object ob)
    {
      if(!logErrors)
        return;
      UnityEngine.Debug.LogError(ob);
      var message = "".Fmt(ob);
      history.Add(new LogEntry(message, LogType.Error));
    }

    public static void Error(string message, params object[] args)
    {
      if(!logErrors)
        return;
      message = message.Fmt(args);
      UnityEngine.Debug.LogError(message);
      history.Add(new LogEntry(message, LogType.Error));
    }

    public static void ClearHistory()
    {
      history.Clear();
    }
  }
}