﻿using System;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace PxlPug.Debug
{
  public class TerminalInput :  IStartable, ITickable, IDisposable
  {
    [Serializable]
    public class Settings
    {
      public KeyCode toggleKey = KeyCode.Slash;
      public KeyCode prevCommandKey = KeyCode.UpArrow;
      public KeyCode nextCommandKey = KeyCode.DownArrow;
    }
    
    private Terminal _terminal;
    private Settings _settings;
    private bool _isTerminalOpen;

    [Inject]
    public TerminalInput(Settings settings, Terminal terminal)
    {
      _settings = settings;
      _terminal = terminal;
    }

    public void Start()
    {
      _terminal.Close();
    }

    public void Tick()
    {
      if(ShouldToggle())
      {
        if(_isTerminalOpen)
          _terminal.Close();
        else
          _terminal.Open();
        _isTerminalOpen = !_isTerminalOpen;
      }

      if(_isTerminalOpen)
      {
        if(GoToPreviousCommand())
          _terminal.PreviousCommand();
        else if(GoToNextCommand())
          _terminal.NextCommand();
      }
    }
    
    private bool ShouldToggle()
    {
      return Input.GetKeyDown(_settings.toggleKey);
    }
    
    private bool GoToPreviousCommand()
    {
      return Input.GetKeyDown(_settings.prevCommandKey);
    }
    
    private bool GoToNextCommand()
    {
      return Input.GetKeyDown(_settings.nextCommandKey);
    }

    public void Dispose()
    {
      _terminal = null;
      _settings = null;
    }
  }
}