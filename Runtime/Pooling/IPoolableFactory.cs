﻿
namespace PxlPug.Pooling
{
  public interface IPoolableFactory
  {
    IPoolable Create();
  }
}