﻿using PxlPug.Debug;
using UnityEngine;

namespace PxlPug
{
  public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
  {
    static T instance;

    bool persist = false;

    public static T Instance
    {
      get
      {
        if(instance == null)
        {
          Log.Info("[Singleton] Finding instance of '" + typeof(T).ToString() + "' object.");
          instance = FindObjectOfType(typeof(T)) as T;
          if(instance == null)
            return null;

          instance.Init();
        }

        return instance;
      }
    }

    public bool Persist
    {
      get { return persist; }
      protected set { persist = value; }
    }

    void Awake()
    {
      if(instance == null)
      {
        instance = this as T;
        instance.Init();
        if(persist)
        {
          transform.parent = null;
          DontDestroyOnLoad(gameObject);
        }
      }
    }

    virtual protected void Init()
    {
    }

    void OnApplicationQuit()
    {
      instance = null;
    }
  }
}