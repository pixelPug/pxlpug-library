﻿using System;

namespace PxlPug
{
  public interface IScheduledAction
  {
    float time { get; set; }
    float targetTime { get; set; }
    int ticks { get; set; }
    int targetTicks { get; set; }
    int repeatCount { get; set; }
    int id { get; set; }
    string tag { get; set; }
    void Invoke();
    bool IsEqual<T>(T t);
  }

  public struct ScheduledAction : IScheduledAction
  {
    public float time { get; set; }
    public float targetTime { get; set; }
    public int ticks { get; set; }
    public int targetTicks { get; set; }
    public int repeatCount { get; set; }
    public int id { get; set; }
    public string tag { get; set; }
    public Action action;

    public ScheduledAction(Action action)
    {
      this.action = action;
      time = 0;
      targetTime = 0;
      ticks = 0;
      targetTicks = 0;
      repeatCount = 1;
      id = 0;
      tag = "";
    }

    public bool IsEqual<T>(T action)
    {
      return this.action.Equals(action);
    }

    public void Invoke()
    {
      action();
    }
  }

  public struct ScheduledAction<TParam1> : IScheduledAction
  {
    public float time { get; set; }
    public float targetTime { get; set; }
    public int ticks { get; set; }
    public int targetTicks { get; set; }
    public int repeatCount { get; set; }
    public int id { get; set; }
    public string tag { get; set; }
    public Action<TParam1> action;
    public TParam1 param1;

    public ScheduledAction(Action<TParam1> action, TParam1 param1)
    {
      this.action = action;
      this.param1 = param1;
      time = 0;
      targetTime = 0;
      ticks = 0;
      targetTicks = 0;
      repeatCount = 1;
      id = 0;
      tag = "";
    }

    public bool IsEqual<T>(T action)
    {
      return this.action.Equals(action);
    }

    public void Invoke()
    {
      action(param1);
    }
  }

  public struct ScheduledAction<TParam1, TParam2> : IScheduledAction
  {
    public float time { get; set; }
    public float targetTime { get; set; }
    public int ticks { get; set; }
    public int targetTicks { get; set; }
    public int repeatCount { get; set; }
    public int id { get; set; }
    public string tag { get; set; }
    public Action<TParam1, TParam2> action;
    public TParam1 param1;
    public TParam2 param2;

    public ScheduledAction(Action<TParam1, TParam2> action, TParam1 param1, TParam2 param2)
    {
      this.action = action;
      this.param1 = param1;
      this.param2 = param2;
      time = 0;
      targetTime = 0;
      ticks = 0;
      targetTicks = 0;
      repeatCount = 1;
      id = 0;
      tag = "";
    }

    public bool IsEqual<T>(T action)
    {
      return this.action.Equals(action);
    }

    public void Invoke()
    {
      action(param1, param2);
    }
  }
}