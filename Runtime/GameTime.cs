﻿using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace PxlPug
{
  public static class GameTime
  {
    public static float timeScale = 1f;
    public static float fixedDeltaTime => Time.fixedDeltaTime * timeScale;
    public static float deltaTime => Time.deltaTime * timeScale;
    public static float time => Time.time * timeScale;
    public static float fixedTime => Time.fixedTime;
    public static int frameCount => Time.frameCount;
  }
}