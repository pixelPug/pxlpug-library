using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PxlPug.Utils
{
  public static class ScreenUtil
  {
    public static Resolution GetResolution()
    {
      #if UNITY_EDITOR
        var viewSize = Handles.GetMainGameViewSize();
        return new Resolution
        {
          width = (int)viewSize.x, 
          height = (int)viewSize.y,
          refreshRate = Screen.currentResolution.refreshRate
        };
      #else
        return Screen.currentResolution;
      #endif
    }
  }  
}