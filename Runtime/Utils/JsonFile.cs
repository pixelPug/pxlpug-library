﻿using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System;
using System.IO.Compression;
using System.Text;
using PxlPug.Debug;

namespace PxlPug.Utils
{
  public static class JsonFile
  {
    private static string _resourcesFolder = "Resources/";
    
    public static T Load<T>(string filePath) where T : class
    {
      AddExtension(ref filePath);
      if(!File.Exists(filePath))
      {
        // Log.Warning("Cannot Load JSON File At " + filePath);
        return null;
      }
      return Deserialize<T>(filePath);
    }

    public static T LoadCompressed<T>(string filePath) where T : class
    {
      AddExtension(ref filePath);
      if(!File.Exists(filePath))
      {
        // Log.Warning("Cannot Load JSON File At " + filePath);
        return null;
      }
      var file = Decompress(File.ReadAllText(filePath));
      return JsonConvert.DeserializeObject<T>(file);
    }
    
    public static string Decompress(string compressedText)
    {
      var gZipBuffer = Convert.FromBase64String(compressedText);
      using (var memoryStream = new MemoryStream())
      {
        int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
        memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);
        var buffer = new byte[dataLength];
        memoryStream.Position = 0;
        using (var gZipStream = new GZipStream(memoryStream,     CompressionMode.Decompress))
        {
          gZipStream.Read(buffer, 0, buffer.Length);
        }
        return Encoding.UTF8.GetString(buffer);
      }
    }
    
    public static object Load(string filePath, Type t)
    {
      AddExtension(ref filePath);
      if(!File.Exists(filePath))
      {
        Log.Warning("Cannot Load JSON File At " + filePath);
        return null;
      }
      var file = File.ReadAllText(filePath);
      return JsonConvert.DeserializeObject(file, t);
    }

    public static object LoadCompressed(string filePath, Type t)
    {
      AddExtension(ref filePath);
      if(!File.Exists(filePath))
      {
        Log.Warning("Cannot Load JSON File At " + filePath);
        return null;
      }
      var file = Decompress(File.ReadAllText(filePath));
      return JsonConvert.DeserializeObject(file, t);
    }

    public static T LoadResource<T>(string filePath) where T : class
    {
      FormatResourceFilePath(ref filePath);
      var asset = Resources.Load<TextAsset>(filePath);
      return JsonConvert.DeserializeObject<T>(asset.text);
    }

    public static T LoadCompressedResource<T>(string filePath) where T : class
    {
      FormatResourceFilePath(ref filePath);
      var asset = Resources.Load<TextAsset>(filePath);
      var uncompressedText = Decompress(asset.text);
      return JsonConvert.DeserializeObject<T>(uncompressedText);
    }

    public static object LoadResource(string filePath, Type t)
    {
      FormatResourceFilePath(ref filePath);
      var asset = Resources.Load<TextAsset>(filePath);
      return JsonConvert.DeserializeObject(asset.text, t);
    }

    private static void FormatResourceFilePath(ref string filePath)
    {
      RemoveResourcesFilePath(ref filePath);
      RemoveFileExtension(ref filePath);
    }

    private static void RemoveResourcesFilePath(ref string filePath)
    {
      var resourcesIndex = filePath.IndexOf(_resourcesFolder);
      if(resourcesIndex != -1)
        filePath = filePath.Substring(resourcesIndex + _resourcesFolder.Length);
    }

    public static object LoadCompressedResource(string filePath, Type t)
    {
      RemoveFileExtension(ref filePath);
      var asset = Resources.Load<TextAsset>(filePath);
      var uncompressedText = Decompress(asset.text);
      return JsonConvert.DeserializeObject(uncompressedText, t);
    }

    private static void AddExtension(ref string filePath)
    {
      if(MissingExtension(filePath))
        filePath += ".json";
    }

    private static T Deserialize<T>(string filePath)
    {
      var file = File.ReadAllText(filePath);
      return JsonConvert.DeserializeObject<T>(file);
    }

    public static string Serialize<T>(T jsonObject, bool prettyFormatting = false)
    {
      var formatting = (prettyFormatting) ? Formatting.Indented : Formatting.None;
      return JsonConvert.SerializeObject(jsonObject, formatting,
        new JsonSerializerSettings()
        {
          ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
          DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate
        });      
    }

    public static void Save<T>(T jsonObject, string filePath, bool prettyFormatting = false)
    {
      var serializedOb = Serialize(jsonObject, prettyFormatting);
      WriteFile(filePath, serializedOb);
    }
    
    public static void SaveResource<T>(T jsonObject, string filePath, bool prettyFormatting = false)
    {
      Save(jsonObject, Application.dataPath + "/Resources/" + filePath, prettyFormatting);
    }

    public static void SaveCompressed<T>(T jsonObject, string filePath)
    {
      var gZipBuffer = ToByteArray(jsonObject);
      WriteFile(filePath, Convert.ToBase64String(gZipBuffer));
    }

    public static void SaveCompressedResource<T>(T jsonObject, string filePath)
    {
      SaveCompressed(jsonObject, Application.dataPath + "/Resources/" + filePath);
    }

    public static byte[] ToByteArray<T>(T jsonObject)
    {
      string serializedOb = JsonConvert.SerializeObject(jsonObject, Formatting.None,
        new JsonSerializerSettings()
        {
          ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
          DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate
        });
      var buffer = Encoding.UTF8.GetBytes(serializedOb);
      var memoryStream = new MemoryStream();
      using (var stream = new GZipStream(memoryStream, CompressionMode.Compress, true))
      {
        stream.Write(buffer, 0, buffer.Length);
      }
      memoryStream.Position = 0;
      var compressed = new byte[memoryStream.Length];
      memoryStream.Read(compressed, 0, compressed.Length);
      var gZipBuffer = new byte[compressed.Length + 4];
      Buffer.BlockCopy(compressed, 0, gZipBuffer, 4, compressed.Length);
      Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer,  0, 4);
      return gZipBuffer;
    }

    public static void Save(object jsonObject, string filePath, bool prettyFormatting = false)
    {
      var formatting = (prettyFormatting) ? Formatting.Indented : Formatting.None;
      string serializedOb = JsonConvert.SerializeObject(jsonObject, formatting,
        new JsonSerializerSettings()
        {
          ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
          DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate
        });
      WriteFile(filePath, serializedOb);
    }
    
    private static void WriteFile(string filePath, string serializedOb)
    {
      AddExtension(ref filePath);
      CheckDirectoryPath(filePath);
      File.WriteAllText(filePath, serializedOb);
    }

    public static void RemoveFileExtension(ref string filePath)
    {
      if(HasExtension(filePath))
        filePath = filePath.Substring(0, filePath.IndexOf(".json"));
    }
    
    private static bool HasExtension(string filePath)
    {
      return filePath.IndexOf(".json") != -1;
    }

    private static bool MissingExtension(string filePath)
    {
      return filePath.IndexOf(".json") == -1;
    }

    private static void CheckDirectoryPath(string filePath)
    {
      var index = filePath.LastIndexOf("/");
      if(index == -1)
        return;

      var directoryPath = filePath.Substring(0, index);
      Directory.CreateDirectory(directoryPath);
    }

    public static T FromJson<T>(string filePath)
    {
      AddExtension(ref filePath);
      return JsonUtility.FromJson<T>(filePath);
    }

    public static void LoadUnityJSON(UnityEngine.Object objectToOverwrite, string filePath)
    {
      AddExtension(ref filePath);
      var file = File.ReadAllText(filePath);
      JsonUtility.FromJsonOverwrite(file, objectToOverwrite);
    }

    public static void SaveUnityJSON(UnityEngine.Object objectToSave, string filePath)
    {
      string serializedOb = JsonUtility.ToJson(objectToSave, true);
      WriteFile(filePath, serializedOb);
    }

    public static bool FileExists(string filePath)
    {
      return File.Exists(filePath);
    }
  }
}
