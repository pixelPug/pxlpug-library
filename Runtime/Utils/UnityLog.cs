using System;
using UnityEngine;

namespace PxlPug.Utils
{
  public static class UnityLog
  {
    private static string _userFolder => Environment.GetFolderPath(Environment.SpecialFolder.Personal);
    private static string _dataFolder => Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
    
    public static string GetEditorLog()
    {
      if (Application.platform == RuntimePlatform.WindowsEditor)
        return @$"{_dataFolder}\Unity\Editor\Editor.log";

      if (Application.platform == RuntimePlatform.OSXEditor)
        return $"{_userFolder}/Library/Logs/Unity/Editor.log";

      return "";
    }
    
    public static string GetPlayerLog()
    {
      var companyName = Application.companyName;
      var productName = Application.productName;

      if (Application.platform == RuntimePlatform.WindowsPlayer)
        return @$"{_dataFolder}\AppData\LocalLow\{companyName}\{productName}\Player.log";

      if (Application.platform == RuntimePlatform.OSXPlayer)
        return $"{_userFolder}/Library/Logs/{companyName}/{productName}/Player.log";

      return "";
    }
  }
}