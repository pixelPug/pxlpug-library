using System;
using System.Linq;
using System.Reflection;

namespace PxlPug.Utils
{
  public class ReflectionUtils
  {
    public static BindingFlags flags = BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty |
                                       BindingFlags.GetField | BindingFlags.SetField | BindingFlags.NonPublic;

    public static PropertyInfo GetPropertyInfo(Type type, string propertyName)
    {
      var props = type.GetProperties(flags);
      return props.FirstOrDefault(propInfo => propInfo.Name == propertyName);
    }

    public static T GetPropertyValue<T>(Type type, string propertyName, object ob)
    {
      return (T) GetPropertyInfo(type, propertyName).GetValue(ob);
    }

    public static FieldInfo GetFieldInfo(Type type, string fieldName)
    {
      var fields = type.GetFields(flags);
      return fields.FirstOrDefault(fieldInfo => fieldInfo.Name == fieldName);
    }
    
    public static T GetFieldValue<T>(Type type, string fieldName, object ob)
    {
      return (T) GetFieldInfo(type, fieldName).GetValue(ob);
    }
  }
}