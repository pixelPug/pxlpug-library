using TMPro;

namespace PxlPug.Extensions
{
  public static class TextMeshProExtensions
  {
    public static void SetAlpha(this TextMeshProUGUI txt, float alpha)
    {
      var color = txt.color;
      color.a = alpha;
      txt.color = color;
    }
    
    public static void SetAlpha(this TextMeshPro txt, float alpha)
    {
      var color = txt.color;
      color.a = alpha;
      txt.color = color;
    }
  }
}