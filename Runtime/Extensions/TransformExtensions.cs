using UnityEngine;

namespace PxlPug.Extensions
{
  public static class TransformExtensions
  {
    private static Transform _trans;
    
    public static Transform GetChild(this Transform trans, string childName)
    {
      for(var i = 0; i < trans.childCount; i++)
      {
        _trans = trans.GetChild(i);
        if(_trans.name == childName)
          return _trans;
      }
      return null;
    }

    public static void SetXScale(this Transform trans, float xScale)
    {
      var scale  = trans.localScale;
      scale.x = xScale;
      trans.localScale = scale;
    }
    
    public static void SetYScale(this Transform trans, float yScale)
    {
      var scale  = trans.localScale;
      scale.y = yScale;
      trans.localScale = scale;
    }

    public static void SetZScale(this Transform trans, float zScale)
    {
      var scale  = trans.localScale;
      scale.z = zScale;
      trans.localScale = scale;
    }
    
    public static void SetXRotation(this Transform trans, float angle)
    {
      var euler = trans.eulerAngles;
      euler.x = angle;
      trans.eulerAngles = euler;
    }
    
    public static void SetYRotation(this Transform trans, float angle)
    {
      var euler = trans.eulerAngles;
      euler.y = angle;
      trans.eulerAngles = euler;
    }
    
    public static void SetZRotation(this Transform trans, float angle)
    {
      var euler = trans.eulerAngles;
      euler.z = angle;
      trans.eulerAngles = euler;
    }

    public static void SetXPosition(this Transform trans, float x)
    {
      var pos = trans.position;
      pos.x = x;
      trans.position = pos;
    }
    
    public static void SetYPosition(this Transform trans, float y)
    {
      var pos = trans.position;
      pos.y = y;
      trans.position = pos;
    }
    
    public static void SetZPosition(this Transform trans, float z)
    {
      var pos = trans.position;
      pos.z = z;
      trans.position = pos;
    }
    
    public static void SetLocalXPosition(this Transform trans, float x)
    {
      var pos = trans.localPosition;
      pos.x = x;
      trans.localPosition = pos;
    }
    
    public static void SetLocalYPosition(this Transform trans, float y)
    {
      var pos = trans.localPosition;
      pos.y = y;
      trans.localPosition = pos;
    }
    
    public static void SetLocalZPosition(this Transform trans, float z)
    {
      var pos = trans.localPosition;
      pos.z = z;
      trans.localPosition = pos;
    }
  }
}