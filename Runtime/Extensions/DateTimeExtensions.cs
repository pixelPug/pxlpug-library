using System;

namespace PxlPug.Extensions
{
  public static class DateTimeExtensions
  {
    // Now
    // var unixTime = DateTime.Now.GetUnixEpoch();
    //
    // 10 Minutes in future:
    // var unixTime2 = (DateTime.Now + new TimeSpan(0, 10, 0)).GetUnixEpoch();
    public static double GetUnixEpoch(this DateTime dateTime)
    {
      var unixTime = dateTime.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
      return unixTime.TotalSeconds;
    }
  }
}
