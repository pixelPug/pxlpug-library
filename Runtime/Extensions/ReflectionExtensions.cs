using System.Reflection;

namespace PxlPug.Extensions
{
  public static class ReflectionExtensions
  {
    public static T GetFieldValue<T>(this object ob, string fieldName)
    {
      var type = ob.GetType();
      var fieldInfo = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
      return (T) fieldInfo?.GetValue(ob);
    }
    
    public static T InvokeMethod<T>(this object ob, string methodName, object[] parms = null)
    {
      var type = ob.GetType();
      var methodInfo = type.GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
      return (T) methodInfo?.Invoke(ob, parms);
    }
    
    public static void InvokeMethod(this object ob, string methodName, object[] parms = null)
    {
      var type = ob.GetType();
      var methodInfo = type.GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
      if(methodInfo != null) methodInfo.Invoke(ob, parms);
    }
  }
}