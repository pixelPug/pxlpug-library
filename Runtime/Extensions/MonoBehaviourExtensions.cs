﻿using System;
using System.Collections.Generic;
using PxlPug.Debug;
using UnityEngine;

namespace PxlPug.Extensions
{
  public static class MonoBehaviourExtensions
  {
    private static Component _comp;
    private static GameObject _child;

    public static T GetComponentOnSelfOrParent<T>(this Component ob) where T : Component
    {
      _comp = ob.GetComponent<T>();
      if(_comp != null)
        return (T) _comp;
      return ob.GetComponentInParent<T>();
    }

    // Get a component on a specific child
    public static T GetComponentOnChild<T>(this Component ob, string childName) where T : class
    {
      _child = ob.gameObject.FindChild(childName);
      if(_child == null)
        return null;
      return _child.GetComponent<T>();
    }

    public static T GetComponentOnChild<T>(this Component ob, string childName, bool searchDisabled) where T : Component
    {
      _child = ob.gameObject.FindChild(childName);
      if(_child == null)
        return null;
      return _child.GetComponentInChildren<T>(searchDisabled);
    }

    // Allows you to search disabled components as well
    public static T GetComponent<T>(this Component ob, bool searchDisabled) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>(searchDisabled);
      if(comps.Length > 0 && comps[0].gameObject == ob)
        return comps[0];
      return null;
    }

    // Allows you to search disabled components as well
    public static T GetComponentInChildren<T>(this Component ob, bool searchDisabled) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>(searchDisabled);
      if(comps.Length < 1)
      {
        // Log.Error("Missing " + typeof(T) + "Component", ob);
        return null;
      }

      return comps[0];
    }

    public static T GetComponentInChildrenOnly<T>(this Component ob) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>();
      if(comps.Length > 1)
        return (comps[0].gameObject == ob) ? comps[1] : comps[0];
      return null;
    }

    public static T GetComponentInChildrenOnly<T>(this Component ob, bool searchDisabled) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>(searchDisabled);
      if(comps.Length > 1)
        return (comps[0].gameObject == ob) ? comps[1] : comps[0];
      return null;
    }

    public static T[] GetComponentsInChildrenOnly<T>(this Component ob) where T : Component
    {
      var comps = new List<T>(ob.GetComponentsInChildren<T>());
      if(comps.Count > 0)
      {
        if(comps[0].gameObject == ob)
          comps.RemoveAt(0);
        return comps.ToArray();
      }

      return comps.ToArray();
    }

    public static T GetOrAddComponent<T>(this Component ob) where T : Component
    {
      _comp = ob.GetComponent<T>();
      if(_comp == null)
        _comp = ob.gameObject.AddComponent<T>();
      return (T) _comp;
    }

    public static Component GetOrAddComponent(this Component ob, string compName)
    {
      _comp = ob.GetComponent(Type.GetType(compName));
      if(_comp == null)
        _comp = ob.gameObject.AddComponent(Type.GetType(compName));
      return _comp;
    }

    public static void RemoveComponent<T>(this Component ob) where T : Component
    {
      _comp = ob.GetComponent<T>();
      if(_comp != null)
        UnityEngine.Object.Destroy(_comp);
    }

    public static void RemoveComponent(this Component ob, string compName)
    {
      _comp = ob.GetComponent(Type.GetType(compName));
      if(_comp != null)
        UnityEngine.Object.Destroy(_comp);
    }

    public static GameObject FindChild(this Component ob, string name)
    {
      Transform[] trans = ob.GetComponentsInChildren<Transform>(true);
      for(var i = 0; i < trans.Length; i++)
      {
        if(trans[i].name == name)
          return trans[i].gameObject;
      }

      Log.Info("null " + name);
      return null;
    }

    public static T FindComponent<T>(this Component ob, string name) where T : Component
    {
      var gameOb = GameObject.Find(name);
      if(gameOb != null)
        return gameOb.GetComponent<T>();
      return null;
    }

    public static T GetInterface<T>(this Component ob) where T : class
    {
      return ob.GetComponent(typeof(T)) as T;
    }
  }
}