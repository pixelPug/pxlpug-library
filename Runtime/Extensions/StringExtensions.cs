using System;
using System.Text.RegularExpressions;
using PxlPug.Utils;

namespace PxlPug.Extensions
{
  public static class StringExtensions
  {
    public static string Fmt(this string s, params object[] args)
    {
      for (int i = 0; i < args.Length; i++)
      {
        var arg = args[i];

        if (arg == null)
          args[i] = "NULL";
        else if (arg is Type)
          args[i] = ((Type)arg).PrettyName();
      }

      return String.Format(s, args);
    }
    
    public static T ToEnum<T>(this string str)
    {
      return (T) Enum.Parse(typeof(T), str);
    }

    public static string ToTitleCase(this string str)
    {
      return str.OnEachWordInString(AsTitle).WithCaptialFirstLetter();
    }

    public static string ToPascalCase(this string str)
    {
      return str.OnEachWordInString(AsPascal).WithCaptialFirstLetter();
    }

    public static string ToCamelCase(this string str)
    {
      return str.OnEachWordInString(AsPascal).WithLowerFirstLetter();
    }

    public static string ToLowerSentenceCase(this string str)
    {
      return str.OnEachWordInString(AsSentence).WithLowerFirstLetter();
    }

    public static string ToSentenceCase(this string str)
    {
      return str.OnEachWordInString(AsSentence).WithCaptialFirstLetter();
    }

    private static string WithCaptialFirstLetter(this string str)
    {
      if(String.IsNullOrWhiteSpace(str)) return str;
      char[] c = str.ToCharArray();
      c[0] = char.ToUpper(c[0]);
      return new string(c);
    }

    private static string WithLowerFirstLetter(this string str)
    {
      if(String.IsNullOrWhiteSpace(str)) return str;
      char[] c = str.ToCharArray();
      c[0] = char.ToLower(c[0]);
      return new string(c);
    }

    private static string OnEachWordInString(this string str, Func<char, char, string> func)
    {
      if(String.IsNullOrWhiteSpace(str)) return str;
      if(str.Contains(" "))
      {
        return str
          .OnEachWordInSpacedString(func)
          .WithCaptialFirstLetter();
      }

      return str.OnEachWordInCasedString(func);
    }

    private static string OnEachWordInCasedString(this string s, Func<char, char, string> eval)
    {
      return Regex.Replace(s.WithCaptialFirstLetter(), "[a-z][A-Z]", m => eval(m.Value[0], m.Value[1]));
    }

    private static string OnEachWordInSpacedString(this string s, Func<char, char, string> eval)
    {
      return Regex.Replace(s.ToLower(), "[a-zA-Z] [a-zA-Z]", m => eval(m.Value[0], m.Value[2]));
    }

    private static Func<char, char, string> AsSentence => (a, b) => char.ToLower(a) + " " + char.ToLower(b);
    private static Func<char, char, string> AsTitle => (a, b) => a + " " + char.ToUpper(b);
    private static Func<char, char, string> AsPascal => (a, b) => a + "" + char.ToUpper(b);
  }
}