﻿using PxlPug.Debug;
using UnityEngine;

namespace PxlPug.Extensions
{
  public static class OrthographicCameraExtensions
  {
    private static Transform _transform;
    private static float _height;
    private static float _nearClipPlane;
    
    public static Vector2 GetMinBounds(this Camera camera)
    {
      Vector3 pos = camera.transform.position;
      return new Vector2(pos.x, pos.y) - camera.Extents();
    }

    public static Vector2 GetMaxBounds(this Camera camera)
    {
      Vector3 pos = camera.transform.position;
      return new Vector2(pos.x, pos.y) + camera.Extents();
    }

    public static Vector2 Extents(this Camera camera)
    {
      if(camera.orthographic)
        return new Vector2(camera.orthographicSize * Screen.width / Screen.height, camera.orthographicSize);
      else
      {
        // Log.Error("Camera is not orthographic!", camera);
        return new Vector2();
      }
    }

    public static Bounds OrthographicBounds(this Camera camera)
    {
      _height = camera.orthographicSize * 2;
      _nearClipPlane = camera.nearClipPlane;
      var depth = camera.farClipPlane - (_nearClipPlane);
      _transform = camera.transform;
      var zOffset = _transform.forward * (_nearClipPlane + (depth / 2f));
      return new Bounds(_transform.position + zOffset, new Vector3(_height * camera.aspect, _height, depth));
    }
  }
}