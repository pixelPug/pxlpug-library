using UnityEngine;

namespace PxlPug.Extensions
{
  public static class Vector2Extensions
  {
    public static Vector2 Rotate(this Vector2 v, float degrees) 
	{
      float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
      float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

      float tx = v.x;
      float ty = v.y;
      v.x = (cos * tx) - (sin * ty);
      v.y = (sin * tx) + (cos * ty);
      return v;
    }

    public static Vector2 RotateAroundPivot(this Vector2 point, Vector2 pivot, Quaternion angle) 
	{
      return angle * ((Vector3)point - (Vector3)pivot) + (Vector3)pivot;
    }

    public static Vector2 RotateAroundPivot(this Vector2 point, Vector2 pivot, Vector2 euler) 
	{
      return RotateAroundPivot(point, pivot, Quaternion.Euler(euler));
    }

    public static bool IsNaN(this Vector2 v)
    {
      return float.IsNaN(v.x) || float.IsNaN(v.y);
    }
  }
}