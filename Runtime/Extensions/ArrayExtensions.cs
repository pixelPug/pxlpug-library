using System;

namespace PxlPug.Extensions
{
  public static class ArrayExtensions
  {
    private static Random rng = new Random();

    public static void Shuffle<T>(this T[] arr)
    {
      int n = arr.Length;
      while (n > 1) {
        n--;
        int k = rng.Next(n + 1);
        T value = arr[k];
        arr[k] = arr[n];
        arr[n] = value;
      }
    }

    public static T[] SubArray<T>(this T[] arr, int index, int length)
    {
      T[] result = new T[length];
      Array.Copy(arr, index, result, 0, length);
      return result;
    }
  }
}
