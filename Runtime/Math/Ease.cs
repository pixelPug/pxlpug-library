using UnityEngine;

namespace PxlPug.Math
{
  public static class Ease
  {
    private const float NATURAL_LOG_OF_2 = 0.693147181f;

    // Progress is 0-1;

    public static float Linear(float start, float end, float progress)
    {
      return Mathf.Lerp(start, end, progress);
    }

    public static float Spring(float start, float end, float progress)
    {
      progress = Mathf.Clamp01(progress);
      progress = (Mathf.Sin(progress * Mathf.PI * (0.2f + 2.5f * progress * progress * progress)) * Mathf.Pow(1f - progress, 2.2f) + progress) * (1f + (1.2f * (1f - progress)));
      return start + (end - start) * progress;
    }

    public static float InQuad(float start, float end, float progress)
    {
      end -= start;
      return end * progress * progress + start;
    }

    public static float OutQuad(float start, float end, float progress)
    {
      end -= start;
      return -end * progress * (progress - 2) + start;
    }

    public static float InOutQuad(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;
      if (progress < 1) return end * 0.5f * progress * progress + start;
      progress--;
      return -end * 0.5f * (progress * (progress - 2) - 1) + start;
    }

    public static float InCubic(float start, float end, float progress)
    {
      end -= start;
      return end * progress * progress * progress + start;
    }

    public static float OutCubic(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return end * (progress * progress * progress + 1) + start;
    }

    public static float InOutCubic(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;
      if (progress < 1) return end * 0.5f * progress * progress * progress + start;
      progress -= 2;
      return end * 0.5f * (progress * progress * progress + 2) + start;
    }

    public static float InQuart(float start, float end, float progress)
    {
      end -= start;
      return end * progress * progress * progress * progress + start;
    }

    public static float OutQuart(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return -end * (progress * progress * progress * progress - 1) + start;
    }

    public static float InOutQuart(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;
      if (progress < 1) return end * 0.5f * progress * progress * progress * progress + start;
      progress -= 2;
      return -end * 0.5f * (progress * progress * progress * progress - 2) + start;
    }

    public static float InQuint(float start, float end, float progress)
    {
      end -= start;
      return end * progress * progress * progress * progress * progress + start;
    }

    public static float OutQuint(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return end * (progress * progress * progress * progress * progress + 1) + start;
    }

    public static float InOutQuint(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;
      if (progress < 1) return end * 0.5f * progress * progress * progress * progress * progress + start;
      progress -= 2;
      return end * 0.5f * (progress * progress * progress * progress * progress + 2) + start;
    }

    public static float InSine(float start, float end, float progress)
    {
      end -= start;
      return -end * Mathf.Cos(progress * (Mathf.PI * 0.5f)) + end + start;
    }

    public static float OutSine(float start, float end, float progress)
    {
      end -= start;
      return end * Mathf.Sin(progress * (Mathf.PI * 0.5f)) + start;
    }

    public static float InOutSine(float start, float end, float progress)
    {
      end -= start;
      return -end * 0.5f * (Mathf.Cos(Mathf.PI * progress) - 1) + start;
    }

    public static float InExpo(float start, float end, float progress)
    {
      end -= start;
      return end * Mathf.Pow(2, 10 * (progress - 1)) + start;
    }

    public static float OutExpo(float start, float end, float progress)
    {
      end -= start;
      return end * (-Mathf.Pow(2, -10 * progress) + 1) + start;
    }

    public static float InOutExpo(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;
      if (progress < 1) return end * 0.5f * Mathf.Pow(2, 10 * (progress - 1)) + start;
      progress--;
      return end * 0.5f * (-Mathf.Pow(2, -10 * progress) + 2) + start;
    }

    public static float InCirc(float start, float end, float progress)
    {
      end -= start;
      return -end * (Mathf.Sqrt(1 - progress * progress) - 1) + start;
    }

    public static float OutCirc(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return end * Mathf.Sqrt(1 - progress * progress) + start;
    }

    public static float InOutCirc(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;
      if (progress < 1) return -end * 0.5f * (Mathf.Sqrt(1 - progress * progress) - 1) + start;
      progress -= 2;
      return end * 0.5f * (Mathf.Sqrt(1 - progress * progress) + 1) + start;
    }

    public static float InBounce(float start, float end, float progress)
    {
      end -= start;
      float d = 1f;
      return end - OutBounce(0, end, d - progress) + start;
    }

    public static float OutBounce(float start, float end, float progress)
    {
      progress /= 1f;
      end -= start;
      if (progress < (1 / 2.75f))
      {
        return end * (7.5625f * progress * progress) + start;
      }
      else if (progress < (2 / 2.75f))
      {
        progress -= (1.5f / 2.75f);
        return end * (7.5625f * (progress) * progress + .75f) + start;
      }
      else if (progress < (2.5 / 2.75))
      {
        progress -= (2.25f / 2.75f);
        return end * (7.5625f * (progress) * progress + .9375f) + start;
      }
      else
      {
        progress -= (2.625f / 2.75f);
        return end * (7.5625f * (progress) * progress + .984375f) + start;
      }
    }

    public static float InOutBounce(float start, float end, float progress)
    {
      end -= start;
      float d = 1f;
      if (progress < d * 0.5f) return InBounce(0, end, progress * 2) * 0.5f + start;
      else return OutBounce(0, end, progress * 2 - d) * 0.5f + end * 0.5f + start;
    }

    public static float InBack(float start, float end, float progress)
    {
      end -= start;
      progress /= 1;
      float s = 1.70158f;
      return end * (progress) * progress * ((s + 1) * progress - s) + start;
    }

    public static float OutBack(float start, float end, float progress)
    {
      float s = 1.70158f;
      end -= start;
      progress = (progress) - 1;
      return end * ((progress) * progress * ((s + 1) * progress + s) + 1) + start;
    }

    public static float InOutBack(float start, float end, float progress)
    {
      float s = 1.70158f;
      end -= start;
      progress /= .5f;
      if ((progress) < 1)
      {
        s *= (1.525f);
        return end * 0.5f * (progress * progress * (((s) + 1) * progress - s)) + start;
      }
      progress -= 2;
      s *= (1.525f);
      return end * 0.5f * ((progress) * progress * (((s) + 1) * progress + s) + 2) + start;
    }

    public static float InElastic(float start, float end, float progress)
    {
      end -= start;

      float d = 1f;
      float p = d * .3f;
      float s;
      float a = 0;

      if (progress == 0) return start;

      if ((progress /= d) == 1) return start + end;

      if (a == 0f || a < Mathf.Abs(end))
      {
        a = end;
        s = p / 4;
      }
      else
      {
        s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
      }

      return -(a * Mathf.Pow(2, 10 * (progress -= 1)) * Mathf.Sin((progress * d - s) * (2 * Mathf.PI) / p)) + start;
    }

    public static float OutElastic(float start, float end, float progress)
    {
      end -= start;

      float d = 1f;
      float p = d * .3f;
      float s;
      float a = 0;

      if (progress == 0) return start;

      if ((progress /= d) == 1) return start + end;

      if (a == 0f || a < Mathf.Abs(end))
      {
        a = end;
        s = p * 0.25f;
      }
      else
      {
        s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
      }

      return (a * Mathf.Pow(2, -10 * progress) * Mathf.Sin((progress * d - s) * (2 * Mathf.PI) / p) + end + start);
    }

    public static float InOutElastic(float start, float end, float progress)
    {
      end -= start;

      float d = 1f;
      float p = d * .3f;
      float s;
      float a = 0;

      if (progress == 0) return start;

      if ((progress /= d * 0.5f) == 2) return start + end;

      if (a == 0f || a < Mathf.Abs(end))
      {
        a = end;
        s = p / 4;
      }
      else
      {
        s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
      }

      if (progress < 1) return -0.5f * (a * Mathf.Pow(2, 10 * (progress -= 1)) * Mathf.Sin((progress * d - s) * (2 * Mathf.PI) / p)) + start;
      return a * Mathf.Pow(2, -10 * (progress -= 1)) * Mathf.Sin((progress * d - s) * (2 * Mathf.PI) / p) * 0.5f + end + start;
    }

    //
    // These are derived functions that the motor can use to get the speed at a specific time.
    //
    // The easing functions all work with a normalized time (0 to 1) and the returned progress here
    // reflects that. Values returned here should be divided by the actual time.
    //
    // TODO: These functions have not had the testing they deserve. If there is odd behavior around
    //       dash speeds then this would be the first place I'd look.

    public static float LinearD(float start, float end, float progress)
    {
      return end - start;
    }

    public static float InQuadD(float start, float end, float progress)
    {
      return 2f * (end - start) * progress;
    }

    public static float OutQuadD(float start, float end, float progress)
    {
      end -= start;
      return -end * progress - end * (progress - 2);
    }

    public static float InOutQuadD(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;

      if (progress < 1)
      {
        return end * progress;
      }

      progress--;

      return end * (1 - progress);
    }

    public static float InCubicD(float start, float end, float progress)
    {
      return 3f * (end - start) * progress * progress;
    }

    public static float OutCubicD(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return 3f * end * progress * progress;
    }

    public static float InOutCubicD(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;

      if (progress < 1)
      {
        return (3f / 2f) * end * progress * progress;
      }

      progress -= 2;

      return (3f / 2f) * end * progress * progress;
    }

    public static float InQuartD(float start, float end, float progress)
    {
      return 4f * (end - start) * progress * progress * progress;
    }

    public static float OutQuartD(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return -4f * end * progress * progress * progress;
    }

    public static float InOutQuartD(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;

      if (progress < 1)
      {
        return 2f * end * progress * progress * progress;
      }

      progress -= 2;

      return -2f * end * progress * progress * progress;
    }

    public static float InQuintD(float start, float end, float progress)
    {
      return 5f * (end - start) * progress * progress * progress * progress;
    }

    public static float OutQuintD(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return 5f * end * progress * progress * progress * progress;
    }

    public static float InOutQuintD(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;

      if (progress < 1)
      {
        return (5f / 2f) * end * progress * progress * progress * progress;
      }

      progress -= 2;

      return (5f / 2f) * end * progress * progress * progress * progress;
    }

    public static float InSineD(float start, float end, float progress)
    {
      return (end - start) * 0.5f * Mathf.PI * Mathf.Sin(0.5f * Mathf.PI * progress);
    }

    public static float OutSineD(float start, float end, float progress)
    {
      end -= start;
      return (Mathf.PI * 0.5f) * end * Mathf.Cos(progress * (Mathf.PI * 0.5f));
    }

    public static float InOutSineD(float start, float end, float progress)
    {
      end -= start;
      return end * 0.5f * Mathf.PI * Mathf.Cos(Mathf.PI * progress);
    }
    public static float InExpoD(float start, float end, float progress)
    {
      return (10f * NATURAL_LOG_OF_2 * (end - start) * Mathf.Pow(2f, 10f * (progress - 1)));
    }

    public static float OutExpoD(float start, float end, float progress)
    {
      end -= start;
      return 5f * NATURAL_LOG_OF_2 * end * Mathf.Pow(2f, 1f - 10f * progress);
    }

    public static float InOutExpoD(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;

      if (progress < 1)
      {
        return 5f * NATURAL_LOG_OF_2 * end * Mathf.Pow(2f, 10f * (progress - 1));
      }

      progress--;

      return (5f * NATURAL_LOG_OF_2 * end) / (Mathf.Pow(2f, 10f * progress));
    }

    public static float InCircD(float start, float end, float progress)
    {
      return ((end - start) * progress) / Mathf.Sqrt(1f - progress * progress);
    }

    public static float OutCircD(float start, float end, float progress)
    {
      progress--;
      end -= start;
      return (-end * progress) / Mathf.Sqrt(1f - progress * progress);
    }

    public static float InOutCircD(float start, float end, float progress)
    {
      progress /= .5f;
      end -= start;

      if (progress < 1)
      {
        return (end * progress) / (2f * Mathf.Sqrt(1f - progress * progress));
      }

      progress -= 2;

      return (-end * progress) / (2f * Mathf.Sqrt(1f - progress * progress));
    }

    public static float InBounceD(float start, float end, float progress)
    {
      end -= start;
      float d = 1f;

      return OutBounceD(0, end, d - progress);
    }

    public static float OutBounceD(float start, float end, float progress)
    {
      progress /= 1f;
      end -= start;

      if (progress < (1 / 2.75f))
      {
        return 2f * end * 7.5625f * progress;
      }
      else if (progress < (2 / 2.75f))
      {
        progress -= (1.5f / 2.75f);
        return 2f * end * 7.5625f * progress;
      }
      else if (progress < (2.5 / 2.75))
      {
        progress -= (2.25f / 2.75f);
        return 2f * end * 7.5625f * progress;
      }
      else
      {
        progress -= (2.625f / 2.75f);
        return 2f * end * 7.5625f * progress;
      }
    }

    public static float InOutBounceD(float start, float end, float progress)
    {
      end -= start;
      float d = 1f;

      if (progress < d * 0.5f)
      {
        return InBounceD(0, end, progress * 2) * 0.5f;
      }
      else
      {
        return OutBounceD(0, end, progress * 2 - d) * 0.5f;
      }
    }

    public static float InBackD(float start, float end, float progress)
    {
      float s = 1.70158f;

      return 3f * (s + 1f) * (end - start) * progress * progress - 2f * s * (end - start) * progress;
    }

    public static float OutBackD(float start, float end, float progress)
    {
      float s = 1.70158f;
      end -= start;
      progress = (progress) - 1;

      return end * ((s + 1f) * progress * progress + 2f * progress * ((s + 1f) * progress + s));
    }

    public static float InOutBackD(float start, float end, float progress)
    {
      float s = 1.70158f;
      end -= start;
      progress /= .5f;

      if ((progress) < 1)
      {
        s *= (1.525f);
        return 0.5f * end * (s + 1) * progress * progress + end * progress * ((s + 1f) * progress - s);
      }

      progress -= 2;
      s *= (1.525f);
      return 0.5f * end * ((s + 1) * progress * progress + 2f * progress * ((s + 1f) * progress + s));
    }

    public static float InElasticD(float start, float end, float progress)
    {
      end -= start;

      float d = 1f;
      float p = d * .3f;
      float s;
      float a = 0;

      if (a == 0f || a < Mathf.Abs(end))
      {
        a = end;
        s = p / 4;
      }
      else
      {
        s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
      }

      float c = 2 * Mathf.PI;

      return ((-a) * d * c * Mathf.Cos((c * (d * (progress - 1f) - s)) / p)) / p -
             5f * NATURAL_LOG_OF_2 * a * Mathf.Sin((c * (d * (progress - 1f) - s)) / p) *
             Mathf.Pow(2f, 10f * (progress - 1f) + 1f);
    }

    public static float OutElasticD(float start, float end, float progress)
    {
      end -= start;

      float d = 1f;
      float p = d * .3f;
      float s;
      float a = 0;

      if (a == 0f || a < Mathf.Abs(end))
      {
        a = end;
        s = p * 0.25f;
      }
      else
      {
        s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
      }

      return (a * Mathf.PI * d * Mathf.Pow(2f, 1f - 10f * progress) *
              Mathf.Cos((2f * Mathf.PI * (d * progress - s)) / p)) / p - 5f * NATURAL_LOG_OF_2 * a *
             Mathf.Pow(2f, 1f - 10f * progress) * Mathf.Sin((2f * Mathf.PI * (d * progress - s)) / p);
    }

    public static float InOutElasticD(float start, float end, float progress)
    {
      end -= start;

      float d = 1f;
      float p = d * .3f;
      float s;
      float a = 0;

      if (a == 0f || a < Mathf.Abs(end))
      {
        a = end;
        s = p / 4;
      }
      else
      {
        s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
      }

      if (progress < 1)
      {
        progress -= 1;

        return -5f * NATURAL_LOG_OF_2 * a * Mathf.Pow(2f, 10f * progress) * Mathf.Sin(2 * Mathf.PI * (d * progress - 2f) / p) -
               a * Mathf.PI * d * Mathf.Pow(2f, 10f * progress) * Mathf.Cos(2 * Mathf.PI * (d * progress - s) / p) / p;
      }

      progress -= 1;

      return a * Mathf.PI * d * Mathf.Cos(2f * Mathf.PI * (d * progress - s) / p) / (p * Mathf.Pow(2f, 10f * progress)) -
             5f * NATURAL_LOG_OF_2 * a * Mathf.Sin(2f * Mathf.PI * (d * progress - s) / p) / (Mathf.Pow(2f, 10f * progress));
    }

    public static float SpringD(float start, float end, float progress)
    {
      progress = Mathf.Clamp01(progress);
      end -= start;

      return end * (6f * (1f - progress) / 5f + 1f) * (-2.2f * Mathf.Pow(1f - progress, 1.2f) *
             Mathf.Sin(Mathf.PI * progress * (2.5f * progress * progress * progress + 0.2f)) + Mathf.Pow(1f - progress, 2.2f) *
             (Mathf.PI * (2.5f * progress * progress * progress + 0.2f) + 7.5f * Mathf.PI * progress * progress * progress) *
             Mathf.Cos(Mathf.PI * progress * (2.5f * progress * progress * progress + 0.2f)) + 1f) -
             6f * end * (Mathf.Pow(1 - progress, 2.2f) * Mathf.Sin(Mathf.PI * progress * (2.5f * progress * progress * progress + 0.2f)) + progress
                         / 5f);

    }
  }
}