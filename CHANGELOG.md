# Changelog
All notable changes to this project will be documented in this file.

## [1.2.0]
### Changed
- Migrated from Zenject to VContainer
- Removed unused pooling classes

## [1.1.5]
### Changed
- Added TextureExtensions

## [1.1.4]
### Changed
- Added ReflectionUtils

## [1.1.3]
### Changed
- Optimizations

## [1.1.2]
### Changed
- Added Project Editor Window

## [1.0.9] - 2020-1-2
### Changed
- Scheduler increased accuracy with time actions

## [1.0.8] - 2019-12-31
### Changed
- Scheduler changed loop order to FIFO

## [1.0.7] - 2019-12-30
### Changed
- TerminalController Added UnRegisterCommand
- TerminalView added null check for EventSystem.current

## [1.0.6] - 2019-12-30
### Changed
- ObjectPool changed from error to warning when size is less than 1

## [1.0.5] - 2019-12-29
### Changed
- CameraExtensions missing using UnityEngine

## [1.0.4] - 2019-12-29
### Changed
- CameraExtensions uses Log instead of Debug.Log

## [1.0.3] - 2019-12-29
### Changed
- Fixed Namespace Issues

## [1.0.2] - 2019-12-28
### Changed
- Removed unused Tests folder
- Package description

## [1.0.0] - 2019-12-28
### Changed
- Organized into UPM structure
- Changed namespaces